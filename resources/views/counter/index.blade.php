@extends('layouts.app', ['active' => 'counter'])

@section('page_header')
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-md-12">
          <form class="search_header" method="POST">
            <table id="table-list" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>LINE</th>
                      <th>ORDER NO</th>
                      <th>SIZE</th>
                      <th>STYLE</th>
                  </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="line_id" id="line_id">
                        <input type="text" class="form-control" name="line" id="line" readonly="readonly" placeholder="PILIH LINE">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_line"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                    </div>
                  </td>
                  <td>
                    <div class="input-group">
                      <div class="input-group-btn">
                          <button type="button" class="btn btn-info"><i class="fa fa-list"></i></button>
                      </div>
                      <input type="text" class="form-control" name="poreference" style="left" id="poreference" onclick="return loadpo()" readonly="readonly" placeholder="PILIH PO BUYER">
                  </td>
                  <td>
                      <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info"><i class="fa fa-list"></i></button>
                        </div>
                        <input type="hidden" name="product_id" id="product_id">
                        <input type="text" class="form-control" name="product" id="product" readonly="readonly" placeholder="PILIH PRODUCT">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-icon" id="clear_product"><span class="text-danger"><span class="fa fa-times"></span></span></button>
                        </div>
                      </div>
                  </td>
                  <td>
                      <div class="input-group">
                        <input type="text" class="form-control" name="style" style="left" id="style" readonly="readonly" placeholder="">
                    </td>
                </tr>
              </tbody>
          </table>
          {{--  <br>
            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> Cari</button>  --}}
        </form>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="table-responsive">
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover table-full-width">
               <thead>
                 <tr>
                   <th width="250px">Nama Proses</th>
                   <th>Mesin</th>
                  <th>NIK</th>
                  <th>Nama Sewer</th>
                </tr>
               </thead>
              <tbody>
                <tr>
                  <td>
                      <div class="input-group">
                          <div class="input-group-btn">
                              <button type="button" class="btn btn-info"><i class="fa fa-list"></i></button>
                          </div>
                          <input type="text" class="form-control" name="proses" style="left" id="proses" readonly="readonly" placeholder="PILIH PROSES">
                      </div>
                  </td>
                  <td>
                      <div class="input-group">
                          <div class="input-group-btn">
                              <button type="button" class="btn btn-info"><i class="fa fa-list"></i></button>
                          </div>
                          <input type="text" class="form-control" name="mesin" style="left" id="mesin" readonly="readonly" placeholder="PILIH MESIN">
                      </div>
                  </td>
                  <td><input type="text" class="form-control" name="nik" id="nik" required autocomplete="off"></td>
                  <td><input type="text" class="form-control" name="sewer" id="sewer" required readonly></td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row col-md-12">
          <div class="col-md-4">
            <button type="button" id="addGood"  class="btn btn-success btn-lg btn-block"><i class="icon-thumbs-up2"></i> GOOD</button>
          </div>
          <div class="col-md-4">
            <button type="button" id="AddDefect" class="btn btn-danger btn-lg btn-block"><i class="icon-thumbs-down2"></i> Defect</button>
          </div>
          <div class="col-md-4">
            <button type="button" id="AddDowntime" class="btn btn-warning btn-lg btn-block"><i class="icon-stop"></i> Mesin Downtime</button>
            <button type="button" id="AddUptime" class="btn btn-info btn-lg btn-block hidden"><i class="icon-play"></i> Mesin Uptime</button>
          </div>
        </div>
    </div>
</div>

@endsection

