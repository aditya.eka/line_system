<!-- MODAL ADD AREA -->
<div id="modal_add_new" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('order.create') }}" id="form-add-new">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="panel-body loader-area">
                      <fieldset>
                          <legend class="text-semibold">
                              <i class="icon-list-ordered position-left"></i>
                              <span id="title"> Tambah Order</span> <!-- title -->
                          </legend>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-12">
                                <label>Po Buyer</label>
                                <input type="text" id="pobuyer" name="pobuyer" class="form-control text-uppercase" required autofocus>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                  <label>Style</label>
                                  <input type="text" id="style" name="style" class="form-control text-uppercase" required>
                                </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                  <label>Job</label>
                                  <input type="text" id="job" name="job" class="form-control text-uppercase" required>
                                </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                  <label>Description</label>
                                  <input type="text" id="description" name="description" class="form-control text-uppercase">
                                </div>
                            </div>
                          </div>
                      </fieldset>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD AREA -->