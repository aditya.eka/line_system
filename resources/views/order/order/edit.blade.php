@extends('layouts.app', ['active' => 'order'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('order.index') }}"><i class="icon-file-media position-left"></i> Edit Master Order</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<!-- 2 columns form -->
<form action="{{ route('order.update') }}" id="main-form" method="POST">
        {{ csrf_field() }}
    <div class="panel panel-flat">
        <div class="panel-body loader-area">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-file-media position-left"></i> # {{ $orders->pobuyer }}</legend>
                        <input type="text" name="id" class="hidden" value="{{ $orders->id }}" readonly>
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label>Po Buyer</label>
                                <input type="text" id="pobuyer" name="pobuyer" class="form-control text-uppercase" value="{{ $orders->pobuyer }}" required>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Style</label>
                                    <input type="text" id="style" name="style" value="{{ $orders->style }}" class="form-control text-uppercase" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Job</label>
                                    <input type="text" id="job" name="job" value="{{ $orders->job }}" class="form-control text-uppercase" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Description</label>
                                    <input type="text" id="description" name="description" value="{{ $orders->description }}" class="form-control text-uppercase">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
                <a class="btn btn-default" href="javascript:history.back()">Close <i class="icon-reload-alt position-right"></i></a>
            </div>
        </div>
    </div>
</form>
<!-- /2 columns form -->

@endsection

@section('js')
<script type="text/javascript">

    $(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#main-form').attr('action'),
            data: $('#main-form').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });
</script>
@endsection