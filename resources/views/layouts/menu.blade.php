<!-- DASHBOARD -->
@permission(['menu-dashboard'])
<li class="{{ isset($active) && $active == 'dashboard' ? 'active' : '' }}">
    <a href="{{ route('dashboard') }}"> <span>DASHBOARD</span></a>
</li>
@endpermission
<!-- END OF DASHBOARD -->

<!-- MENU ADMIN -->
@permission(['menu-user-management'])
<li class="navigation-header"><span>User Management</span><i class="icon-menu" title="User Management"></i></li>
<li class="{{ isset($active) && $active == 'user' ? 'active' : '' }}">
    <a href="{{ route('user.index') }}"><i class="icon-users"></i><span>User</span></a>
</li>
<li class="{{ isset($active) && $active == 'roles' ? 'active' : '' }}">
    <a href="{{ route('role.index') }}"><i class="glyphicon glyphicon-repeat"></i><span>Roles</span></a>
</li>
@endpermission
<!-- END MENU ADMIN -->

<!-- MENU MASTER MANAGEMENT -->
@permission(['menu-master'])
<li class="navigation-header"><span>Master Management</span><i class="icon-menu" title="Master Management"></i></li>
<li class="{{ isset($active) && $active == 'operator' ? 'active' : '' }}">
    <a href="{{ route('operator.index') }}"><i class="icon-users"></i><span>Operator</span></a>
</li>
<li class="{{ isset($active) && $active == 'line' ? 'active' : '' }}">
    <a href="{{ route('line.index') }}"><i class="icon-link2"></i><span>Line</span></a>
</li>
<li class="{{ isset($active) && $active == 'mesin' ? 'active' : '' }}">
    <a href="{{ route('mesin.index') }}"><i class="icon-shredder"></i><span>Mesin</span></a>
</li>
<li class="{{ isset($active) && $active == 'proses' ? 'active' : '' }}">
    <a href="{{ route('proses.index') }}"><i class="icon-power3"></i><span>Proses</span></a>
</li>
<li class="{{ isset($active) && $active == 'defect' ? 'active' : '' }}">
    <a href="{{ route('defect.index') }}"><i class="icon-warning22"></i><span>Defect</span></a>
</li>
<li class="{{ isset($active) && $active == 'product' ? 'active' : '' }}">
    <a href="{{ route('product.index') }}"><i class="icon-file-media"></i><span>Product</span></a>
</li>

@endpermission
<!-- END OF MENU USER MANAGEMENT -->

<!-- MENU ORDER MANAGEMENT -->
@permission(['menu-order', 'menu-planning'])
<li class="navigation-header"><span>Order Management</span><i class="icon-menu" title="Order Management"></i></li>
<li class="{{ isset($active) && $active == 'order' ? 'active' : '' }}">
    <a href="{{ route('order.index') }}"><i class="icon-list-ordered"></i><span>Order</span></a>
</li>
@endpermission
<!-- END OF MENU ORDER MANAGEMENT -->

<!-- MENU INLINE -->
@permission(['menu-inline'])
<li class="navigation-header"><span>Operator Counter Process</span><i class="icon-menu" title="Operator Counter Process"></i></li>
<li class="{{ isset($active) && $active == 'counter' ? 'active' : '' }}">
    <a href="{{ route('counter.index') }}"><i class="icon-touch-pinch"></i><span>Counter</span></a>
</li>
@endpermission
<!-- END OF MENU INLINE -->