<!-- MODAL ADD AREA -->
<div id="modal_add_new" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('defect.create') }}" id="form-add-new">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="panel-body loader-area">
                      <fieldset>
                          <legend class="text-semibold">
                              <i class="icon-warning22 position-left"></i>
                              <span id="title"> Tambah Defect</span> <!-- title -->
                          </legend>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-12">
                                <label>Type Defect</label>
                                <select name="defect_type" id="defect_type" class="form-control" required>
                                  @foreach ($defects as $key => $value)
                                    <option value="{{ $defects[$key] }}">{{ $defects[$value] }}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-12">
                                <label>Nama Defect</label>
                                <input type="text" id="defect_name" name="defect_name" class="form-control" required>
                              </div>
                            </div>
                          </div>
                      </fieldset>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD AREA -->