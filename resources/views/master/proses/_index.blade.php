<!-- MODAL ADD AREA -->
<div id="modal_add_new" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('proses.create') }}" id="form-add-new">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="panel-body loader-area">
                      <fieldset>
                          <legend class="text-semibold">
                              <i class="icon-power3 position-left"></i>
                              <span id="title"> Tambah Proses</span> <!-- title -->
                          </legend>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-12">
                                <label>Nama Proses</label>
                                <input type="text" id="name" name="name" class="form-control" required>
                              </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                  <label>DESCRIPTION</label>
                                  <input type="text" id="description" name="description" class="form-control" required>
                                </div>
                            </div>
                          </div>
                      </fieldset>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD AREA -->