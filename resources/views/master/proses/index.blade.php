@extends('layouts.app', ['active' => 'proses'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('proses.index') }}"><i class="icon-power3 position-left"></i> Master Proses</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group" style="float:right">
            <button type="button" class="btn btn-xs btn-primary add_new"><i class="icon-plus2"></i> ADD NEW</button>
        </div>
    </div>
    <div class="panel-body">
        <table class="table datatable-save-state" id="table-list">
            <thead>
                <tr>
                    <th style="width:10px;">#</th>
                    <th>NAMA</th>
                    <th>CREATED DATE</th>
                    <th>DESCRIPTION</th>
                    <th style="width:10px;">ACTION</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<!-- IMPORTANT LINK -->
<a href="{{ route('proses.getDatatables') }}" id="get_data"></a>
<!-- /IMPORTANT LINK -->
@endsection

@section('modal')
	@include('master.proses._index')
@endsection

@section('js_extension')
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		var url = $('#get_data').attr('href');

		// datatables
		$.extend( $.fn.dataTable.defaults, {
	        autoWidth: false,
	        autoLength: false,
	        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
	        language: {
	            search: '<span>Filter:</span> _INPUT_',
	            searchPlaceholder: 'Type to filter...',
	            lengthMenu: '<span>Show:</span> _MENU_',
	            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
	        }
	    });

	    var table = $('#table-list').DataTable({
	        ajax: url,
	        fnCreatedRow: function (row, data, index) {
	            var info = table.page.info();
	            var value = index+1+info.start;
	            $('td', row).eq(0).html(value);
	        },
	        columns: [
	            {data: null, sortable: false, orderable: false, searchable: false  },
                {data: 'name', name: 'name'},
                {data: 'created_at', name: 'created_at'},
                {data: 'description', name: 'description'},
	            {data: 'action', name: 'action', orderable: false, searchable: false}
	        ]
	    });
	    //end of datatables


		$('.add_new').on('click', function() {
			$('#modal_add_new').modal('show');
		});

		$('#modal_add_new').on('hidden.bs.modal',function() {
			$('#form-add-new').trigger("reset");
			$('#name').val('').trigger('change');
		});

		$('#form-add-new').submit(function(event) {
			event.preventDefault();
			$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	        $.ajax({
	        	url: $('#form-add-new').attr('action'),
	        	type: 'POST',
	        	data: $('#form-add-new').serialize(),
	        	beforeSend: function () {
	        		$('.loader-area').block({
	                    message: '<i class="icon-spinner4 spinner"></i>',
	                    overlayCSS: {
	                        backgroundColor: '#fff',
	                        opacity: 0.8,
	                        cursor: 'wait'
	                    },
	                    css: {
	                        border: 0,
	                        padding: 0,
	                        backgroundColor: 'none'
	                    }
	                });
	        	},
	        	complete: function () {
	            	$('.loader-area').unblock();
	            },
	            success: function(response) {
	                myalert('success','GOOD');
	                $('#form-add-new').trigger("reset");
	                $('#modal_add_new').trigger('toggle');
	                table.ajax.reload();
	            },
	            error: function(response) {
	                myalert('error','NOT GOOD');
	            }
	        });

		});

		 //delete master
	    $("#table-list").on("click", ".delete", function() {
	        event.preventDefault();
	        var id = $(this).data('id');
	        if(id == '') {
	            return false;
	        }

	        var token = $(this).data("token");
	        bootbox.confirm("Are you sure delete this row ?", function (result) {
	            if (result) {
	                $.ajaxSetup({
	                    headers: {
	                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                    }
	                });

	                $.ajax({
	                    url: "proses/delete-proses/"+id,
	                    type: "GET",
	                    data: {
	                        "id": id,
	                        "_method": 'DELETE',
	                        "_token": token,
	                    },
	                    beforeSend: function () {
	                        $('.loader-area').block({
	                            message: '<i class="icon-spinner4 spinner"></i>',
	                            overlayCSS: {
	                                backgroundColor: '#fff',
	                                opacity: 0.8,
	                                cursor: 'wait'
	                            },
	                            css: {
	                                border: 0,
	                                padding: 0,
	                                backgroundColor: 'none'
	                            }
	                        });
	                    },
	                    complete: function () {
	                        $(".loader-area").unblock();
	                    },
	                    success: function () {
	                        myalert('success','Data has been deleted');
	                        table.ajax.reload();
	                    }
	                });
	            }
	        });
	    });
	    //end of delete

	});

</script>
@endsection