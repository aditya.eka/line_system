@extends('layouts.app', ['active' => 'line'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('line.index') }}"><i class="icon-link2 position-left"></i> Edit Master Line</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<!-- 2 columns form -->
<form action="{{ route('line.update') }}" id="main-form" method="POST">
        {{ csrf_field() }}
    <div class="panel panel-flat">
        <div class="panel-body loader-area">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-line position-left"></i> # {{ $lines->name }}</legend>
                        <input type="text" name="id" class="hidden" value="{{ $lines->id }}" readonly>
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label>Nama Mesin</label>
                                <input type="text" id="name" name="name" class="form-control text-uppercase" value="{{ $lines->name }}" required autofocus>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>DESCRIPTION</label>
                                    <input type="text" id="description" name="description" value="{{ $lines->description }}" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
                <a class="btn btn-default" href="javascript:history.back()">Close <i class="icon-reload-alt position-right"></i></a>
            </div>
        </div>
    </div>
</form>
<!-- /2 columns form -->

@endsection

@section('js')
<script type="text/javascript">

    $(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#main-form').attr('action'),
            data: $('#main-form').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error', response['responseJSON']);
            }
        })
    });
</script>
@endsection