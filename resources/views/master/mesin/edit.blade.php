@extends('layouts.app', ['active' => 'mesin'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('mesin.index') }}"><i class="icon-shredder position-left"></i> Edit Master Mesin</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<!-- 2 columns form -->
<form action="{{ route('mesin.update') }}" id="main-form" method="POST">
        {{ csrf_field() }}
    <div class="panel panel-flat">
        <div class="panel-body loader-area">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-shredder position-left"></i> # {{ $mesins->name }}</legend>
                        <input type="text" name="id" class="hidden" value="{{ $mesins->id }}" readonly>
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label>Nama Mesin</label>
                                <input type="text" id="name" name="name" class="form-control text-uppercase" value="{{ $mesins->name }}" required>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>DESCRIPTION</label>
                                    <input type="text" id="description" name="description" value="{{ $mesins->description }}" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Line</label>
                                    <select name="line_id" id="line_id" class="form-control select-search" required>
                                    <option value=""></option>
                                    @foreach ($lines as $line)
                                        <option value="{{ $line->id }}" {{ $mesins->line_id == $line->id ? 'selected' : '' }}>{{ $line->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
                <a class="btn btn-default" href="javascript:history.back()">Close <i class="icon-reload-alt position-right"></i></a>
            </div>
        </div>
    </div>
</form>
<!-- /2 columns form -->

@endsection

@section('js')
<script type="text/javascript">

    $(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#main-form').attr('action'),
            data: $('#main-form').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });
</script>
@endsection