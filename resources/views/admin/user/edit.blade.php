@extends('layouts.app', ['active' => 'user'])
@section('content')

<br>
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('user.index') }}"><i class="icon-home2 position-left"></i>User Management</a></li>
    </ul>
</div>

<section class="panel">
    <div class="panel-body loader-area">
        <form action="{{ route('user.update') }}" id="main-form" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
            <fieldset>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-default border">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-semibold">USER FORM</h6>
                                </div>
                                <input type="text" name="id" class="hidden" value="{{ $user->id }}" id="user-form">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label text-semibold">USER :</label>
                                        <input type="text" name="user_id" placeholder="Username" class="form-control" id="user_id" value="{{ $user->user_id }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label text-semibold">NAME :</label>
                                        <input type="text" name="name" placeholder="Name" class="form-control" id="name" value="{{ $user->name }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label text-semibold">EMAIL :</label>
                                        <input type="email" name="email" placeholder="E-mail" class="form-control" id="email" value="{{ $user->email }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-default border">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-semibold">USER ROLE &nbsp; <span class="label label-info heading-text">Mapping User with Role.</span></h6>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>SELECT ROLE</th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="role_id" id="role_id" required>
                                                        @foreach($roles as $r)
                                                            @if($r->id == $user_roles)
                                                            <option value="{{$r->id}}" selected>{{$r->display_name}}</option>
                                                            @else
                                                            <option value="{{$r->id}}" >{{$r->display_name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
                            <div id="pass" class="hidden">
                                <label class="control-label col-md-2 text-uppercase">Password</label>
                                <input type="text" value="1234" class="form-control" readonly disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <a href="{{ route('role.index') }}" id="url_role" class="hidden"></a>
            <div class="form-group text-right" style="margin-right: 1%;">
                <a href="{{ route('user.index') }}" class="btn btn-danger btn-sm">BACK <i class="icon-arrow-left16 position-right"></i></a>
                <button type="button" class="btn btn-warning" id="reset-password" name="reset">RESET PASSWORD <i class="icon-lock4 position-right"></i></button>
                <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
            </div>
        </form>
    </div>
</section>
<a href="{{ route('user.resetPassword', $user->id) }}" id="url_reset_password"></a>
<a href="{{ route('deliveryorder.ajaxGetDataDistrict') }}" id="get_data_district"></a>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {

    var validator = null;

    var url_district = $('#get_data_district').attr('href');

    $('#areas_id').change(function(event) {
        if ($(this).val() != '') {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url_district,
                type: 'GET',
                data: {areas_id: $(this).val()},
                success: function (response) {
                    $('#districts_id').find('option').remove();
                    $('#districts_id').append('<option value=""></option>');
                    $.each(response, function(index, value) {
                        $('#districts_id').append('<option value="'+value.districts_id+'">'+value.districts_name+'</option>');
                    });

                },
                error: function (response) {
                    myalert('error','Oops Something wrong..');
                }
            });
        }
    });

    $(".save-data").on("click", function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $("#main-form").attr("action"),
            data: $("#main-form").serialize(),
            beforeSend: function () {
                $(".loader-area").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $(".loader-area").unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $("#main-form").trigger("reset");
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

    $("#reset-password").on("click", function () {
        var fd = $("#main-form").serialize();
        var url = $("#url_reset_password").attr('href');
        var id = $("#user-form").val();
        console.log(url);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url : url,
            data: {id: id},
            dataType: 'json',
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                $('#pass').removeClass('hidden');
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

});
</script>
@endsection
