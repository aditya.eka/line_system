@extends('layouts.app', ['active' => 'dashboard'])

@section ('page_header')
<!-- Page header -->
<div class="page-header">
    <br>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home4 position-left"></i> Dashboard</a></li>
        </ul>
    </div>
</div>
<br>
<!-- /page header -->
@endsection

@section('content')
<!-- STATUS -->
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-list2 icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_order">{{ isset($total_order) ? $total_order : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Order</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-thumbs-down2 icon-3x text-danger-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_defect">{{ isset($total_defect) ? $total_defect : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Defect</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-thumbs-up2 icon-3x text-blue-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_good">{{ isset($total_good) ? $total_good : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Good</span>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="clearfix w-100 d-md-none pb-20">
<!-- /STATUS -->
@endsection