<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($edit))
               <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete))
                <li><a href="{{ $delete }}" data-id="{{ isset($id) ? $id : 'kosong' }}" class="ignore-click delete"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_user))
                <li><a href="{{ $delete_user }}" data-userid="{{ isset($userid) ? $userid : 'kosong' }}" class="ignore-click deleteUser"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_product))
                <li><a href="{{ $delete_product }}" data-productid="{{ isset($productid) ? $productid : '' }}" class="ignore-click deleteProduct"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_role))
                <li><a href="{{ $delete_role }}" data-roleid="{{ isset($roleid) ? $roleid : 'kosong' }}" class="ignore-click deleteRole"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($edit_categories))
               <li><a href="{!! $edit_categories !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_categories))
                <li><a href="{{ $delete_categories }}" data-id="{{ isset($categoriesid) ? $categoriesid : '' }}" class="ignore-click deleteCategories"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($edit_brands))
               <li><a href="{!! $edit_brands !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_brands))
                <li><a href="{{ $delete_brands }}" data-id="{{ isset($brandsid) ? $brandsid : '' }}" class="ignore-click deleteBrands"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($edit_jenis))
               <li><a href="{!! $edit_jenis !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_jenis))
                <li><a href="{{ $delete_jenis }}" data-id="{{ isset($jenisid) ? $jenisid : '' }}" class="ignore-click deleteJenis"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($edit_uoms))
               <li><a href="{!! $edit_uoms !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_uoms))
            <li><a href="{{ $delete_uoms }}" data-id="{{ isset($uomsid) ? $uomsid : '' }}" class="ignore-click deleteUoms"><i class="icon-close2"></i> Delete</a></li>
            @endif
            
            @if (isset($edit_operator))
               <li><a href="{!! $edit_operator !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_operator))
                <li><a href="{{ $delete_operator }}" data-id="{{ isset($id) ? $id : 'kosong' }}" class="ignore-click deleteOperator"><i class="icon-close2"></i> Delete</a></li>
            @endif
        </ul>
    </li>
</ul>
