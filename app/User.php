<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'id','name', 'user_id', 'nik', 'email', 'password', 'created_at', 'updated_at','deleted_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isOnline()
	{
	    return Cache::has('user-is-online-' . $this->id);
	}
}
