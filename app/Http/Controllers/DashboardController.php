<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Excel;
use Carbon\Carbon;

class DashboardController extends Controller
{
    //
    public function index(Request $request)
    {
    	return view('dashboard/index');
    }
}
