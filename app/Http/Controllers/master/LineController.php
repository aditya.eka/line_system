<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;
use DataTables;
use App\Models\Line;

class LineController extends Controller
{
    //
    public function index() {
        return view('master.line.index');
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $lines = Line::orderBy('created_at', 'desc')
                            ->get();

            //datatables
            return DataTables::of($lines)
            ->addColumn('action', function($lines){ 
                return view('_action', [
                                        'edit' => route('line.edit', $lines->id),
                                        'id' => $lines->id,
                                        'delete' => route('line.delete', $lines->id)
                                        ]);
            })
            ->make(true);
        }
    }


    public function create(Request $request){

        $data=$request->all();


        $data = new Line;
        $data->name = trim(strtoupper($request['name']));
        $data->description = $request['description'];
        $data->created_at = carbon::now();

        // cek nama line sudah ada atau belum
        $cek = Line::where('name', $data->name)
                    ->exists();
        
        if ($cek) {
            return response()->json('Line Already Exists..!', 422);
        }


        $data->save();
        
    }

    public function edit($id){
        $lines = Line::find($id);
        return view('master.line.edit', compact('lines'));
    }

    public function update(Request $request){
        $data = $request->all();

        $lines['name'] = trim(strtoupper($data['name']));
        $lines['updated_at'] = Carbon::now();
        $lines['description'] = $data['description'];

        // cek nama line sudah ada atau belum
        $cek = Line::where('name', $lines['name'])
                    ->where('id', '<>', $data['id'])
                    ->exists();
        
        if ($cek) {
            return response()->json('Line Already Exists..!', 422);
        }

        $lines = DB::table('master_line')
                        ->where('id', $data['id'])
                        ->update($lines);
    }

    public function delete(Request $request)
    {   
        $id = $request->id;
        Line::find($id)->delete();
    }
}
