<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;
use DataTables;
use App\Models\Proses;

class ProsesController extends Controller
{
    //
    public function index() {
        return view('master.proses.index');
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $proses = Proses::orderBy('created_at', 'desc')
                            ->get();

            //datatables
            return DataTables::of($proses)
            ->addColumn('action', function($proses){ 
                return view('_action', [
                                        'edit' => route('proses.edit', $proses->id),
                                        'id' => $proses->id,
                                        'delete' => route('proses.delete', $proses->id)
                                        ]);
            })
            ->make(true);
        }
    }


    public function create(Request $request){

        $data=$request->all();


        $data = new Proses;
        $data->name = $request['name'];
        $data->description = $request['description'];
        $data->created_at = carbon::now();
        $data->save();
        
    }

    public function edit($id){
        $proses = Proses::find($id);
        return view('master.proses.edit', compact('proses'));
    }

    public function update(Request $request){
        $data = $request->all();

        $proses['name'] = $data['name'];
        $proses['updated_at'] = Carbon::now();
        $proses['description'] = $data['description'];

        $proses = DB::table('master_proses')
                        ->where('id', $data['id'])
                        ->update($proses);
    }

    public function delete(Request $request)
    {   
        $id = $request->id;
        Proses::find($id)->delete();
    }
}
