<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Mesin;
use Carbon\Carbon;
use DB;
use DataTables;

use App\Models\Line;

class MesinController extends Controller
{
    //
    public function index() {
        $lines = Line::all();
        return view('master.mesin.index', compact('lines'));
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $mesins = Mesin::orderBy('created_at', 'desc')
                            ->get();

            //datatables
            return DataTables::of($mesins)
            ->addColumn('action', function($mesins){ 
                return view('_action', [
                                        'edit' => route('mesin.edit', $mesins->id),
                                        'id' => $mesins->id,
                                        'delete' => route('mesin.delete', $mesins->id)
                                        ]);
            })
            ->make(true);
        }
    }


    public function create(Request $request){

        $data=$request->all();

        $data = new Mesin;
        $data->name = trim(strtoupper($request['name']));
        $data->description = $request['description'];
        $data->line_id = $request['line_id'];
        $data->created_at = carbon::now();

        // cek apakah nama mesin sudah ada atau belum
        $cek = Mesin::where('name', $data->name)
                        ->exists();
        if ($cek) {
            return response()->json('Mesin Already Exists..!', 422);
        }

        $data->save();
        
    }

    public function edit($id){
        $mesins = Mesin::find($id);
        $lines = Line::all();
        return view('master.mesin.edit', compact('mesins', 'lines'));
    }

    public function update(Request $request){
        $data = $request->all();

        $mesins['name'] = trim(strtoupper($data['name']));
        $mesins['updated_at'] = Carbon::now();
        $mesins['description'] = $data['description'];
        $mesins['line_id'] = $data['line_id'];

        // cek apakah nama mesin sudah ada atau belum
        $cek = Mesin::where('name', $mesins['name'])
                        ->where('id', '<>', $data['id'])
                        ->exists();
        if ($cek) {
            return response()->json('Mesin Already Exists..!', 422);
        }

        $mesins = DB::table('master_mesin')
                        ->where('id', $data['id'])
                        ->update($mesins);
    }

    public function deleteMesin(Request $request)
    {   
        $id = $request->id;
        Mesin::find($id)->delete();
    }
}
