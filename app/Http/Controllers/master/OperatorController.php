<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Operator;
use DB;
use DataTables;
use Carbon\Carbon;
use Auth;

class OperatorController extends Controller
{
    public function index() {
        return view('master.operator.operator');
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
        $operators = DB::table('master_operator')
             ->select('id','nik','name', 'created_at','updated_at','description')
             ->orderBy('created_at', 'desc')
             ->get();

         //datatables
         return DataTables::of($operators)
           ->addColumn('action', function($operators){ return view('_action', [
             'edit' => route('operator.edit', $operators->id),
             'id' => $operators->id,
             'delete_operator' => route('operator.delete', $operators->id)]);})
         ->make(true);
     }
 }


 public function create(Request $request){

     $data=$request->all();


    $data = new operator;
    $data->name = $request['name'];
    $data->nik = $request['nik'];
    $data->description = $request['description'];
    $data->created_at = carbon::now();
    $data->save();
    
 }

 public function edit($id){
    $operators = Operator::find($id);
     return view('master.operator.edit', compact('operators'));
 }

 public function update(Request $request){
     $data = $request->all();

     $operators['nik'] = $data['nik'];
     $operators['name'] = $data['name'];
     $operators['updated_at'] = Carbon::now();
     $operators['description'] = $data['description'];

     $operators = DB::table('master_operator')
                     ->where('id', $data['id'])
                     ->update($operators);
 }

 public function delete_operator(Request $request)
 {   
     $id = $request->id;
     $operators = Operator::find($id)->delete();
 }
}
