<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;
use DataTables;
use App\Models\Product;

class ProductController extends Controller
{
    //
    public function index() {
        return view('master.product.index');
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $products = Product::orderBy('created_at', 'desc')
                            ->get();

            //datatables
            return DataTables::of($products)
            ->addColumn('action', function($products){ 
                return view('_action', [
                                        'edit' => route('product.edit', $products->id),
                                        'id' => $products->id,
                                        'delete' => route('product.delete', $products->id)
                                        ]);
            })
            ->make(true);
        }
    }


    public function create(Request $request){

        $data=$request->all();

        $data = new Product();
        $data->name = trim(strtoupper($request['name']));
        $data->size = trim(strtoupper($request['size']));
        $data->created_at = carbon::now();
        $data->save();
        
    }

    public function edit($id){
        $products = Product::find($id);
        return view('master.product.edit', compact('products'));
    }

    public function update(Request $request){
        $data = $request->all();

        $products['name'] = trim(strtoupper($data['name']));
        $products['updated_at'] = Carbon::now();
        $products['size'] = trim(strtoupper($data['size']));

        $products = DB::table('master_product')
                        ->where('id', $data['id'])
                        ->update($products);
    }

    public function delete(Request $request)
    {   
        $id = $request->id;
        Product::find($id)->delete();
    }
}
