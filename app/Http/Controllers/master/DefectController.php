<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;
use DataTables;
use App\Models\Defect;

use Config;

class DefectController extends Controller
{
    //
    public function index() {
        $defects = Config::get('constants.defects');
        // dd($defects);
        return view('master.defect.index', compact('defects'));
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $defects = Defect::orderBy('created_at', 'desc')
                            ->get();

            //datatables
            return DataTables::of($defects)
            ->addColumn('action', function($defects){ 
                return view('_action', [
                                        'edit' => route('defect.edit', $defects->id),
                                        'id' => $defects->id,
                                        'delete' => route('defect.delete', $defects->id)
                                        ]);
            })
            ->make(true);
        }
    }


    public function create(Request $request){

        $data=$request->all();


        $data = new Defect;
        $data->defect_name = $request['defect_name'];
        $data->defect_type = $request['defect_type'];
        $data->created_at = carbon::now();
        $data->save();
        
    }

    public function edit($id){
        $defect = Config::get('constants.defects');
        $defects = Defect::find($id);
        return view('master.defect.edit', compact('defects', 'defect'));
    }

    public function update(Request $request){
        $data = $request->all();

        $defects['defect_name'] = $data['defect_name'];
        $defects['updated_at'] = Carbon::now();
        $defects['defect_type'] = $data['defect_type'];

        $defects = DB::table('master_defect')
                        ->where('id', $data['id'])
                        ->update($defects);
    }

    public function delete(Request $request)
    {   
        $id = $request->id;
        Defect::find($id)->delete();
    }
}
