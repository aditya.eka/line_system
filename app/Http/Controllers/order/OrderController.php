<?php

namespace App\Http\Controllers\order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;
use DataTables;
use App\Models\Order;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    //
    public function index() {
        return view('order.order.index');
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $orders = Order::orderBy('created_at', 'desc')
                            ->get();

            //datatables
            return DataTables::of($orders)
            ->addColumn('action', function($orders){ 
                return view('_action', [
                                        'edit' => route('order.edit', $orders->id),
                                        'id' => $orders->id,
                                        'delete' => route('order.delete', $orders->id)
                                        ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request){

        $data=$request->all();

        $data = new Order();
        $data->pobuyer = trim(strtoupper($request['pobuyer']));
        $data->style = trim(strtoupper($request['style']));
        $data->job = trim(strtoupper($request['job']));
        $data->description = trim(strtoupper($request['description']));
        $data->created_at = carbon::now();

        // cek po buyer sudah ada atau belum
        $cek = Order::where('pobuyer', $data->pobuyer)
                    ->exists();
        
        if ($cek) {
            return response()->json('PO Buyer Already Exists..!', 422);
        }

        $data->save();
        
    }

    public function edit($id){
        $orders = Order::find($id);
        return view('order.order.edit', compact('orders'));
    }

    public function update(Request $request){
        $data = $request->all();

        $orders['pobuyer'] = trim(strtoupper($data['pobuyer']));
        $orders['updated_at'] = Carbon::now();
        $orders['style'] = trim(strtoupper($data['style']));
        $orders['job'] = trim(strtoupper($data['job']));
        $orders['description'] = trim(strtoupper($data['description']));

        // cek po buyer sudah ada atau belum
        $cek = Order::where('pobuyer', $orders['pobuyer'])
                    ->where('id', '<>', $data['id'])
                    ->exists();
        
        if ($cek) {
            return response()->json('PO Buyer Already Exists..!', 422);
        }

        $orders = DB::table('master_order')
                        ->where('id', $data['id'])
                        ->update($orders);
    }

    public function delete(Request $request)
    {   
        $id = $request->id;
        $order = Order::find($id);

        if ($order->delete()) {
            OrderDetail::where('order_id', $id)->delete();
        };
    }
}
