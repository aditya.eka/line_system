<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $user_id  = request()->get('user_id');
        $fieldName = 'user_id';
        request()->merge([$fieldName => $user_id]);
        return $fieldName;
    }
    /**
    * Validate the user login.
    * @param Request $request
    */
    public function login(Request $request){
        $this->validate(
            $request,
            [
                'user_id' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'user_id.required' => 'User is required',
                'password.required' => 'Password is required',
            ]
        );
        $getRoles = \DB::table('role_user')
                        ->select('roles.name','users.deleted_at')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->join('users', 'users.id', '=', 'role_user.user_id')
                        ->where('users.user_id', $request->user_id)
                        // ->wherenull('users.deleted_at')
                        ->first();


        if(auth()->attempt(['user_id'=>$request->user_id,'password'=>$request->password])){

            if(auth()->user()->deleted_at!==null){
                Auth::logout();
                return redirect()->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        'password' => 'Your account has not yet been activated',
                    ]);
            }

                return redirect(route('dashboard'));

        }else {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => trans('auth.failed'),
                ]);
        }
    }


    /**
    * @param Request $request
    * @throws ValidationException
    */

    protected function authenticated(Request $request, $user)
    {

        $getRoles = \DB::table('role_user')
                        ->select('roles.name','users.deleted_at')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->join('users', 'users.id', '=', 'role_user.user_id')
                        ->where('role_user.user_id', $user->id)
                        // ->wherenull('users.deleted_at')
                        ->first();

            return redirect()->route('dashboard');

    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));


        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                'password' => trans('auth.failed'),
            ]);

    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}
