<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'master_order';

    
    public function orderdetail(){
        return $this->hasMany('App\Models\OrderDetail', 'order_id', 'id');
    }
}
