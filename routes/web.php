<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//lOGIN
Route::get('/', function () {
    if(Auth::check()) {

        return redirect()->route('dashboard');
    }
    return redirect('/login');
});

//LOGOUT DARURAT ONLY
Route::get('/logoutdarurat', function() {
    if(Auth::check()) {
        Auth::logout();
    }
    return redirect('/');
});

Route::get('/home', function() {
    if(Auth::check()) {
        return redirect('/');
    }
    return redirect('/login');
});

Auth::routes();

Route::middleware('auth')->group(function () {

	//DASHBOARD
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    //account setting
    Route::get('/user/account-setting/{id}', 'Admin\UserController@accountSetting')->name('user.accountSetting');
    Route::post('/user/account-setting/update-password/{id}', 'Admin\UserController@updatepassword')->name('user.updatepassword');

    //ROUTE ADMIN
    Route::prefix('admin')->group(function () {
        Route::middleware('role:admin')->group(function () {

            //user management
            Route::get('/user', 'Admin\UserController@index')->name('user.index');
            Route::get('/user/getdatatables', 'Admin\UserController@getDatatables')->name('user.getDatatables');
            Route::get('/user/add-user', 'Admin\UserController@create')->name('user.create');
            Route::get('/user/edit-user/{id}', 'Admin\UserController@edit')->name('user.edit');
            Route::post('/user/user-store', 'Admin\UserController@store')->name('user.store');
            Route::post('/user/update-user', 'Admin\UserController@update')->name('user.update');
            Route::get('/user/edit-user/{id}/reset-password', 'Admin\UserController@resetPassword')->name('user.resetPassword');
            Route::get('/user/delete-user/{id}', 'Admin\UserController@destroy')->name('user.delete');
            Route::get('/user-role', 'Admin\UserController@role')->name('user.role');

            //role
            Route::get('/role', 'Admin\RoleController@index')->name('role.index');
            Route::get('/role/getdatatables', 'Admin\RoleController@getDatatables')->name('role.getDatatables');
            Route::get('/role/add-role', 'Admin\RoleController@create')->name('role.create');
            Route::post('/role/role-store', 'Admin\RoleController@store')->name('role.store');
            Route::get('/role/edit-role/{id}', 'Admin\RoleController@edit')->name('role.edit');
            Route::post('/role/update-role', 'Admin\RoleController@update')->name('role.update');
            Route::get('/role/delete-role/{id}', 'Admin\RoleController@delete')->name('role.delete');
        });
    });

    
    Route::prefix('master')->group(function () {
        //operator
        Route::get('/operator', 'master\OperatorController@index')->name('operator.index');
        Route::get('/operator/getdatatables', 'master\OperatorController@getDatatables')->name('operator.getDatatables');
        Route::post('/operator/add-operator', 'master\OperatorController@create')->name('operator.create');
        Route::get('/operator/edit-operator/{id}', 'master\OperatorController@edit')->name('operator.edit');
        Route::post('/operator/operator-store', 'master\OperatorController@store')->name('operator.store');
        Route::post('/operator/update-operator', 'master\OperatorController@update')->name('operator.update');
        Route::get('/operator/delete_operator/{id}', 'master\OperatorController@delete_operator')->name('operator.delete');
        // Route::get('/operator-role', 'master\OperatorController@role')->name('operator.role');

        //mesin
        Route::get('/mesin', 'master\MesinController@index')->name('mesin.index');
        Route::get('/mesin/getdatatables', 'master\MesinController@getDatatables')->name('mesin.getDatatables');
        Route::post('/mesin/add-mesin', 'master\MesinController@create')->name('mesin.create');
        Route::get('/mesin/edit-mesin/{id}', 'master\MesinController@edit')->name('mesin.edit');
        Route::post('/mesin/update-mesin', 'master\MesinController@update')->name('mesin.update');
        Route::get('/mesin/delete-mesin/{id}', 'master\MesinController@deleteMesin')->name('mesin.delete');

        //proses
        Route::get('/proses', 'master\ProsesController@index')->name('proses.index');
        Route::get('/proses/getdatatables', 'master\ProsesController@getDatatables')->name('proses.getDatatables');
        Route::post('/proses/add-proses', 'master\ProsesController@create')->name('proses.create');
        Route::get('/proses/edit-proses/{id}', 'master\ProsesController@edit')->name('proses.edit');
        Route::post('/proses/update-proses', 'master\ProsesController@update')->name('proses.update');
        Route::get('/proses/delete-proses/{id}', 'master\ProsesController@delete')->name('proses.delete');

        //line
        Route::get('/line', 'master\LineController@index')->name('line.index');
        Route::get('/line/getdatatables', 'master\LineController@getDatatables')->name('line.getDatatables');
        Route::post('/line/add-line', 'master\LineController@create')->name('line.create');
        Route::get('/line/edit-line/{id}', 'master\LineController@edit')->name('line.edit');
        Route::post('/line/update-line', 'master\LineController@update')->name('line.update');
        Route::get('/line/delete-line/{id}', 'master\LineController@delete')->name('line.delete');

        //defect
        Route::get('/defect', 'master\DefectController@index')->name('defect.index');
        Route::get('/defect/getdatatables', 'master\DefectController@getDatatables')->name('defect.getDatatables');
        Route::post('/defect/add-defect', 'master\DefectController@create')->name('defect.create');
        Route::get('/defect/edit-defect/{id}', 'master\DefectController@edit')->name('defect.edit');
        Route::post('/defect/update-defect', 'master\DefectController@update')->name('defect.update');
        Route::get('/defect/delete-defect/{id}', 'master\DefectController@delete')->name('defect.delete');
        
        //product
        Route::get('/product', 'master\ProductController@index')->name('product.index');
        Route::get('/product/getdatatables', 'master\ProductController@getDatatables')->name('product.getDatatables');
        Route::post('/product/add-product', 'master\ProductController@create')->name('product.create');
        Route::get('/product/edit-product/{id}', 'master\ProductController@edit')->name('product.edit');
        Route::post('/product/update-product', 'master\ProductController@update')->name('product.update');
        Route::get('/product/delete-product/{id}', 'master\ProductController@delete')->name('product.delete');
    });
    
    Route::prefix('order')->group(function () {
        //order
        Route::get('/', 'order\OrderController@index')->name('order.index');
        Route::get('/getdatatables', 'order\OrderController@getDatatables')->name('order.getDatatables');
        Route::post('/add-order', 'order\OrderController@create')->name('order.create');
        Route::get('/edit-order/{id}', 'order\OrderController@edit')->name('order.edit');
        Route::post('/update-order', 'order\OrderController@update')->name('order.update');
        Route::get('/delete-order/{id}', 'order\OrderController@delete')->name('order.delete');

    });
    
    Route::prefix('counter')->group(function () {
        //operator
        Route::get('/counter', 'CounterController@index')->name('counter.index');
        
    });
    
});
