<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles  = Role::Select('id')->get();

        $user = new User();
        $user->name = 'Administrator';
        $user->nik = 'admin';
        $user->user_id = 'admin';
        $user->email = 'admin@mail.com';
        $user->password =  bcrypt('admin08');
        $user->created_at =  Carbon::now();

        if($user->save())
            $user->attachRoles($roles);
    }
}
