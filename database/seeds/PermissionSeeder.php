<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
            [
                'name' => 'menu-dashboard',
                'display_name' => 'menu dashboard',
                'description' => 'menu dashboard'
            ],
            [
                'name' => 'menu-user-management',
                'display_name' => 'menu user management',
                'description' => 'menu user management'
            ],
            [
                'name' => 'menu-master',
                'display_name' => 'menu master',
                'description' => 'menu master'
            ],
            [
                'name' => 'menu-inline',
                'display_name' => 'menu inline',
                'description' => 'menu inline'
            ],
            [
                'name' => 'menu-order',
                'display_name' => 'menu order',
                'description' => 'menu order'
            ],
            [
                'name' => 'menu-planning',
                'display_name' => 'menu planning',
                'description' => 'menu planning'
            ],
            [
                'name' => 'menu-report',
                'display_name' => 'menu report',
                'description' => 'menu report'
            ],
            
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
