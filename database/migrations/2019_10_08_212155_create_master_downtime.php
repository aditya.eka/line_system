<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterDowntime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_downtime', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('line_id');
            $table->integer('proses_id');
            $table->integer('mesin_id');
            $table->dateTime('downtime');
            $table->dateTime('uptime');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_downtime');
    }
}
