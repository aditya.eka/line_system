<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_order', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('pobuyer','255');
            $table->string('style','255');
            $table->string('job','255');
            $table->string('description','255')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_order');
    }
}
